require "aws-record"

module Models
	class Song
		include Aws::Record
		set_table_name "Song"
		
		integer_attr :id, hash_key: true
		string_attr :artist
		string_attr :title
	end
end