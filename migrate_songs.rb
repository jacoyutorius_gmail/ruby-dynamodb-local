require "aws-sdk"
require "aws-record"
require "awesome_print"
require "hashie"
require "./song"

module Models

	class Migrate 
	 attr_accessor :client, :migration

	 def initialize
	 	Aws.config.update(endpoint: "http://localhost:8000")
		@client = Aws::DynamoDB::Client.new
		@migration = Aws::Record::TableMigration.new(Song, client: client)
	 end

	 def up
		unless Song.table_exists?
			migration.create!(
				table_name: "Song", # <= なんか効かない
				provisioned_throughput: {
					read_capacity_units: 5,
					write_capacity_units: 2
				}
			)
			migration.wait_until_available
		end
	 end

	 def down
	 	 migration.delete! if Song.table_exists?
	 end
	end
end




if __FILE__ == $0
	# p Models::Migrate.new.down
	p Models::Migrate.new.up
end