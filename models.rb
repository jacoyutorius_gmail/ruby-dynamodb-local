require "aws-record"

# class Song
# 	include Aws::Record
# 	integer_attr :id, hash_key: true
# 	string_attr :artist
# 	string_attr :title
# end

module Models
	class Song
		include Aws::Record
		integer_attr :id, hash_key: true
		string_attr :artist
		string_attr :title
	end
end