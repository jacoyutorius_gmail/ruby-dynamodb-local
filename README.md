
## dynamodb-local のインストール

```
mkdir dynamodb_local
wget https://s3-ap-northeast-1.amazonaws.com/dynamodb-local-tokyo/dynamodb_local_latest.tar.gz -P dynamodb_local
tar -xzvf dynamodb_local/dynamodb_local_latest.tar.gz -C dynamodb_local

echo '#!/bin/bash\njava -Djava.library.path=./dynamodb_local/DynamoDBLocal_lib -jar dynamodb_local/DynamoDBLocal.jar -sharedDb' >> dynamodb_local/start.sh
chmod 700 start.sh
bash dynamodb_local/start.sh
```

#### 起動確認

http://localhost:8000/shell/# 

## 

```bash
bundle exec ruby migrate_songs.rb
```

## Pry

```bash
bundle exec pry -r "aws-sdk" -r "aws-record" -r "awesome_print" -r "./migrate_songs"
```

↑を改修

```bash
bundle exec pry -r "./pry_config"
```


```ruby
Models::Song.configure_client(endpoint: "http://localhost:8000")
data = [
	{ artist: "小林太郎", title: "Armour Zone" },
	{ artist: "プライマル・スクリーム", title: "Where The Light Gets in" },
	{ artist: "The Birthday", title: "I KNOW" },
]
data.each_with_index do |row, i|
	song = Models::Song.new
	song.id = i + 1
	song.artist = row[:artist]
	song.title = row[:title]
	song.save
end

scan = Models::Song.scan
scan.each do |r|
  ap "#{r.title}/#{r.artist}"
end
```


```ruby
Models::Song.new(id: 5, artist: "Red Hot Chili Peppers", title: "Can't Stop").save!
Models::Song.scan.count
Models::Song.scan.each{|r| p r.title }
```



```ruby
column_names = Models::Song.attributes.attributes.each_key.map{|r| r }
Models::Song.scan.each{|row| column_names.each{|col| ap eval("row.#{col}") } }
ap Models::Song.scan.map{|row| column_names.map{|col| eval("row.#{col}") } }
```

